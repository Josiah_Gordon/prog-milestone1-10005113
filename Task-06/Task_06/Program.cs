﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int read;
            int sum = 0;
            Console.WriteLine("Type in 5 numbers. ");
            for (int i = 0; i < 5; i++)
            {
                read = int.Parse(Console.ReadLine());
                sum = sum + read;
            }

            Console.WriteLine("The sum of the 5 numbers is " + sum);
        }
    }
}