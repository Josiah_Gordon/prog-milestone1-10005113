﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {
            double month = 31;
            double total = 0;
            double wednesdays1 = 0;
            int wednesdays2 = 0;

            total = month - 2;
            wednesdays1 = total / 7;
            wednesdays2 = Convert.ToInt32(wednesdays1);

            Console.WriteLine($"There are {wednesdays2} wednesdays in a 31 day month.");
        }
    }
}
