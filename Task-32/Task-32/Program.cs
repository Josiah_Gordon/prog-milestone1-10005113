﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32

{
    class Program
    {
        static void Main(string[] args)
        {
            string[] strings = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };


            Console.WriteLine("The numbers each day of the week are");

            for (int i = strings.GetLowerBound(0); i <= strings.GetUpperBound(0); i++)
                Console.WriteLine("{0} {1}", i + 1, strings[i]);

        }
    }
}