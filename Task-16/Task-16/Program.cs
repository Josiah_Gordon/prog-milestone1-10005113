﻿using System;

class Sample
{
    public static void Main()
    {
        Console.WriteLine("enter a word to be counted");

        string str = Console.ReadLine();
        Console.WriteLine("The length of '{0}' is {1}", str, str.Length);
    }
}