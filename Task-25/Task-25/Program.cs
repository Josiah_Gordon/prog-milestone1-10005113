﻿using System;
class SumDoubles
{
    static void Main()
    {
        double DblSumTotal = 0;
        double LIMIT = 0;


        Console.Clear();
        Console.WriteLine("Enter a number.");
        do
        {
            double d;
            if (!double.TryParse(Console.ReadLine(), out d))
            {
                Console.WriteLine("error! enter a number then press enter.");
            }
            else
            {
                DblSumTotal = DblSumTotal + d;
                LIMIT = LIMIT + 1;
            }

        } while (LIMIT < 1);

        Console.WriteLine("Your number is " + DblSumTotal);
    }
}