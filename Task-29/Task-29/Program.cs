﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program
    {
        static void Main(string[] args)
        {
            string st1 = "The quick brown fox jumped over the fence";
            char[] seperator = new char[] { ' ' };
            int numberOfWords = st1.Split(seperator, StringSplitOptions.RemoveEmptyEntries).Length + 1;
            Console.WriteLine("Number of words in '{1}' is {0}", numberOfWords, st1);
        }
    }
}
