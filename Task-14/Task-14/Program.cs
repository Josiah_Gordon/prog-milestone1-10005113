﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            double GB = 0;
            double TB = 0;
            double Multi = 1024;

            Console.WriteLine("Type in computer size in TB. ");
            TB = double.Parse(Console.ReadLine());
            GB = TB * Multi;
            Console.WriteLine($"The storage size is  {GB}GB." );
        }
    }
}