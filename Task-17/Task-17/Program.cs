﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var Name = new Tuple<string, int, string, int, string, int>
                      ("Ed", 11, "Edd", 10, "Eddy", 10);
            Console.WriteLine("{0} {1}, {2} {3}, {4} {5}",
                              Name.Item1, Name.Item2, Name.Item3,
                              Name.Item4, Name.Item5, Name.Item6);
        }
    }
}
