﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = new Dictionary<string, int>();

            month.Add("January", 31);
            month.Add("Febuary", 28);
            month.Add("March", 31);
            month.Add("April", 30);
            month.Add("May", 31);
            month.Add("June", 30);
            month.Add("July", 31);
            month.Add("August", 30);
            month.Add("September", 31);
            month.Add("October", 30);
            month.Add("Noember", 31);
            month.Add("December", 30);


            var months = new List<string>();


            foreach(var myMonth in month)
{
                if(myMonth.Value == 31)
    {
                    months.Add(myMonth.Key);
                }
            }

            var temp = String.Join("，", months);
            Console.WriteLine($"Months with 31 Days are: {temp}");


        }

    }
}