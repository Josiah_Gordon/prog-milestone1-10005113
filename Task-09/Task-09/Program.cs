﻿using System;

public class IsLeapYear
{
    public static void Main()
    {
        for (int year = 2016; year <= 2026; year++)
        {
            if (DateTime.IsLeapYear(year))
            {
                Console.WriteLine("{0} is a leap year.", year);
                DateTime leapDay = new DateTime(year, 2, 29);
                DateTime nextYear = leapDay.AddYears(1);

            }
        }
    }
}