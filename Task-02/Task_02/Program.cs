﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type the month and day you were born");

            var name = Console.ReadLine();
            Console.WriteLine($"Hello you were born on {name}!");
        }
    }
}
