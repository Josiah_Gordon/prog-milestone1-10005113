﻿using System;
class SumDoubles
{
    static void Main()
    {
        double DblSumTotal = 0;
        double LIMIT = 0;


        Console.Clear();
        Console.WriteLine("Enter 3 numbers to be counted +gst.");
        do
        {
            double d;
            if (!double.TryParse(Console.ReadLine(), out d))
            {
                Console.WriteLine("error! enter a number then press enter.");
            }
            else {
                DblSumTotal = DblSumTotal + d;
                LIMIT = LIMIT + 1;
            }

        } while (LIMIT < 3);

        Console.WriteLine("The total sum of the 3 numbers plus gst is $" + DblSumTotal * 1.15);
        Console.ReadLine();
    }
}